package customer;

import customer.models.Customer;
import customer.repositories.CustomerRepository;
import customer.repositories.impl.CustomerJdbcRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.util.List;

@SpringBootTest
class TechMahindraApplicationTests {

    @Autowired
	CustomerJdbcRepository customerJdbcRepository;

	@Before
	public void setup() {
		EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
				.setType(EmbeddedDatabaseType.H2)
				.addScript("schema.sql")
				.build();
	}

	@Test
	public void testJdbcTempalte() {
		Customer customer = new Customer(0, "hayslan1@msn.com", "hayslan", "Prychwa", "00182604039", "Haven Street");
		customerJdbcRepository.create(customer);

		List<Customer> loadedPersons = customerJdbcRepository.getAll();
		System.out.println("Loaded Customer: " + loadedPersons);
		for (Customer loadedPerson : loadedPersons) {
			Assert.assertTrue("hayslan1@msn.com".equals(loadedPerson.getLast_name()));
			Assert.assertTrue("hayslan".equals(loadedPerson.getEmail_address()));
			Assert.assertTrue("Prychwa".equals(loadedPerson.getFirst_name()));
			Assert.assertTrue("00182604039".equals(loadedPerson.getDocument()));
			Assert.assertTrue("Haven Street".equals(loadedPerson.getAddress()));
		}
	}

}
