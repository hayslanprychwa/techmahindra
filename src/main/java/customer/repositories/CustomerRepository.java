package customer.repositories;


import customer.models.Customer;
import java.util.List;

public interface CustomerRepository {

    public List<Customer> getAll();
    public void create(Customer customer);
    public void update(Customer customer);
    public void delete(Long customer_id);

}
