package customer.repositories.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import customer.models.Customer;
import customer.repositories.CustomerRepository;

import java.util.List;


@Repository
public class CustomerJdbcRepository implements CustomerRepository {

    @Autowired
    @Qualifier("jdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CustomerJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public List<Customer> getAll() {
        String sql = ("SELECT * FROM CUSTOMER;");
        return jdbcTemplate.query(sql,
                (resultSet, rowNum) ->
                    new Customer(
                    		 resultSet.getInt("customer_id"),
                             resultSet.getString("first_name"),
                             resultSet.getString("last_name"),
                             resultSet.getString("email_address"),
                             resultSet.getString("document"),
                             resultSet.getString("address")

                    ));
    }

    @Override
    public void create(Customer customer) {
        String sql = "INSERT INTO CUSTOMER" +
                "(first_name, last_name, email_address, document, address) VALUES (?, ?, ?, ?, ?);";

        jdbcTemplate.update(sql, new Object[] {customer.getFirst_name(), customer.getLast_name(), customer.getEmail_address(), customer.getDocument(), customer.getAddress() });
    }
    
    @Override
    public void update(Customer customer) {
        String sql = "UPDATE CUSTOMER SET first_name = ?, last_name = ?, email_address = ?, document = ?, address =? WHERE customer_id = ?;";
        jdbcTemplate.update(sql, new Object[] {customer.getFirst_name(), customer.getLast_name(), customer.getEmail_address(), customer.getDocument(), customer.getAddress(), customer.getCustomer_id() });
    }
    
    @Override
    public void delete(Long customer_id) {
        String sql = "DELETE FROM CUSTOMER WHERE customer_id = ?";

        jdbcTemplate.update(sql, customer_id );
    }
    
}
