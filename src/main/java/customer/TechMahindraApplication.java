package customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechMahindraApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechMahindraApplication.class, args);
	}

}
