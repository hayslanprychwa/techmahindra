package customer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import customer.models.Customer;
import customer.repositories.CustomerRepository;
import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(value="/findAll", method=RequestMethod.GET)
    public List<Customer> getAllUsers() {
        return customerRepository.getAll();
    }

    @RequestMapping(value ="/create", method=RequestMethod.POST)
    public @ResponseBody
    List<Customer> addCustomer(@RequestBody Customer jsonString) {
        customerRepository.create(jsonString);
        return getAllUsers();
    }

    @RequestMapping(value ="/update", method=RequestMethod.PUT)
    public @ResponseBody
    List<Customer> updateCustomer(@RequestBody Customer customer) {
        customerRepository.update(customer);
        return getAllUsers();
    }

    @RequestMapping(value ="/delete/{customer_id}", method=RequestMethod.DELETE)
    public @ResponseBody
    List<Customer> delCustomer(@PathVariable Long customer_id) {
        customerRepository.delete(customer_id);
        return getAllUsers();
    }

}