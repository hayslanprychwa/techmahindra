package customer.models;

import java.io.Serializable;

public class Customer implements Serializable {

	 	private Integer customer_id;
	    private String first_name;
	    private String last_name;
	    private String email_address;
	    private String document;
	    private String address;

	    public Customer(Integer customer_id, String email_address, String first_name, String last_name,
				String document, String address) {
	    	super();
			this.customer_id = customer_id;
			this.first_name = first_name;
			this.last_name = last_name;
			this.email_address = email_address;
			this.document = document;
			this.address = address;
		}


    public Integer getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void create(Customer customer) {
	}

	@Override
	public String toString() {
		return "Customer{" +
				"customer_id=" + customer_id +
				", first_name='" + first_name + '\'' +
				", last_name='" + last_name + '\'' +
				", email_address='" + email_address + '\'' +
				", document='" + document + '\'' +
				", address='" + address + '\'' +
				'}';
	}

}

